﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Blog.Models;
using Blog.Db;

namespace Blog.Controllers
{
   
    public class PostDetalle
    {
        public Post Post { get; set; }
        public List<Comentario> Comentarios { get; set; }
    }
    public class HomeController : Controller
    {
        
        private PostContext context;
        public HomeController(PostContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var bloggers = context.Bloggers;
            var orden = context.Bloggers.OrderByDescending(o=>o.Fecha);
            return View(orden);
        }

        public IActionResult nuevoBlog()
        {

            return View();
        }

        [HttpPost]
        public ActionResult nuevoBlog(Post po)
        {
            po.Fecha = DateTime.Now;
            context.Bloggers.Add(po);
            context.SaveChanges();
            return RedirectToAction("Index");

           // DateTime fecha = DateTime.Today;

        }
        [HttpGet]
        public ViewResult detallePost(int id)
        {
            var bloggers = context.Bloggers;

           
            Post post = bloggers.FirstOrDefault(item => item.Id == id);
            List<Comentario> comentarios = context.Comentarios.Where(o => o.PostId == id).OrderByDescending(o => o.Fech).ToList();

   

            // DateTime fecha = DateTime.Today;

            var detalle = new PostDetalle
            {
                Post = post,
                Comentarios = comentarios
            };

            return View(detalle);
        }

        public RedirectToActionResult AddComentario(Comentario comentario)
        {
            comentario.Fech = DateTime.Now;
           
            context.Comentarios.Add(comentario);
            context.SaveChanges();
            return RedirectToAction("detallePost", new { id = comentario.PostId });
        }

    }
}
