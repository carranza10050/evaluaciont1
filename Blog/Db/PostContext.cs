﻿using Blog.Db.Mapping;
using Blog.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Db
{
    public class PostContext : DbContext
    {
        public DbSet<Post> Bloggers { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }

        public PostContext(DbContextOptions<PostContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new PostMap());
            modelBuilder.ApplyConfiguration(new ComentarioMap());

        }
    }
}
